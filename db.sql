
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

DELIMITER $$

CREATE DEFINER=`root`@`%` PROCEDURE `count_all_orders` ()  SELECT COUNT(id) as all_count FROM orders$$

CREATE DEFINER=`root`@`%` PROCEDURE `count_filtered_orders` (IN `search_value` VARCHAR(100))  SELECT count(orders.id) as filtered_count FROM `orders`
INNER JOIN users on users.id=orders.user_id
WHERE users.user_name LIKE CONCAT('%',search_value,'%')
or orders.id=search_value
order by orders.id DESC$$

CREATE DEFINER=`root`@`%` PROCEDURE `orders_list` (IN `start_data` INT, IN `length_data` INT)  SELECT users.user_name,users.id as user_id, orders.* FROM `orders`
INNER JOIN users on users.id=orders.user_id
order by orders.id DESC
limit start_data,length_data$$

CREATE DEFINER=`root`@`%` PROCEDURE `orders_list_filtered` (IN `start_data` INT, IN `length_data` INT, IN `search_value` VARCHAR(100))  SELECT users.user_name, users.id as user_id, orders.* FROM `orders`
INNER JOIN users on users.id=orders.user_id
WHERE users.user_name LIKE CONCAT('%',search_value,'%')
or orders.id=search_value
order by orders.id DESC
limit start_data,length_data$$

DELIMITER ;

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_price` double NOT NULL,
  `date_of_payment` datetime NOT NULL,
  `date_of_creation` datetime NOT NULL,
  `order_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `orders` (`id`, `user_id`, `order_price`, `date_of_payment`, `date_of_creation`, `order_status`) VALUES
(1, 1, 350.94831, '2018-08-01 00:00:00', '2018-10-12 00:00:00', 0),
(2, 2, 0, '2018-02-15 00:00:00', '2018-08-17 00:00:00', 0),
(3, 3, 9044.791, '2018-08-04 00:00:00', '2019-01-02 00:00:00', 0),
(4, 4, 435924052.58593, '2018-11-02 00:00:00', '2018-05-02 00:00:00', 0),
(5, 5, 0, '2018-10-10 00:00:00', '2018-11-07 00:00:00', 0),
(6, 6, 22855.7561444, '2018-05-27 00:00:00', '2018-08-26 00:00:00', 0),
(7, 7, 47980.156243396, '2018-11-02 00:00:00', '2018-02-19 00:00:00', 0),
(8, 8, 5.179179, '2018-12-13 00:00:00', '2018-11-15 00:00:00', 0),
(9, 9, 244649316.1, '2018-07-04 00:00:00', '2018-05-19 00:00:00', 0),
(10, 10, 3363207.839, '2018-12-17 00:00:00', '2018-02-12 00:00:00', 0),
(11, 11, 0, '2018-08-27 00:00:00', '2018-11-14 00:00:00', 0),
(12, 12, 194.126, '2018-12-18 00:00:00', '2018-11-13 00:00:00', 0),
(14, 14, 788562790.68158, '2018-09-20 00:00:00', '2018-09-27 00:00:00', 0),
(15, 15, 37329807, '2018-11-05 00:00:00', '2019-01-07 00:00:00', 0),
(16, 16, 11.9293, '2018-05-19 00:00:00', '2018-09-20 00:00:00', 0),
(17, 17, 31.4, '2018-12-27 00:00:00', '2018-03-10 00:00:00', 0),
(18, 18, 134.62783, '2018-06-22 00:00:00', '2018-11-09 00:00:00', 0),
(19, 19, 55507963.7, '2018-08-24 00:00:00', '2018-07-15 00:00:00', 0),
(20, 20, 56.8746, '2019-01-02 08:21:19', '2018-08-15 00:00:00', 1),
(21, 21, 29484783, '2018-07-12 00:00:00', '2018-12-09 00:00:00', 0),
(22, 22, 189702653.22, '2018-06-07 00:00:00', '2018-10-07 00:00:00', 0),
(23, 23, 0, '2018-10-28 00:00:00', '2018-12-15 00:00:00', 0),
(24, 24, 8063303.657, '2018-12-20 00:00:00', '2018-06-16 00:00:00', 0),
(25, 25, 1084, '2018-11-28 00:00:00', '2018-06-13 00:00:00', 0),
(26, 26, 848715.8761, '2018-10-11 00:00:00', '2018-05-15 00:00:00', 0),
(27, 27, 4865036.2666655, '2018-04-01 00:00:00', '2018-08-11 00:00:00', 0),
(28, 28, 15124, '2018-08-14 00:00:00', '2018-11-27 00:00:00', 0),
(29, 29, 78868.81, '2018-11-01 00:00:00', '2018-08-10 00:00:00', 0),
(30, 30, 3.6807, '2018-04-12 00:00:00', '2018-12-07 00:00:00', 0),
(31, 31, 604.88, '2018-09-11 00:00:00', '2018-12-24 00:00:00', 0),
(32, 32, 0, '2018-09-06 00:00:00', '2018-02-09 00:00:00', 0),
(33, 33, 1099730.505318, '2018-12-09 00:00:00', '2018-06-07 00:00:00', 0),
(34, 34, 165762853.799, '2018-09-13 00:00:00', '2018-09-29 00:00:00', 0),
(35, 35, 0, '2018-09-02 00:00:00', '2018-02-24 00:00:00', 0),
(36, 36, 76917.064, '2018-03-14 00:00:00', '2018-11-19 00:00:00', 0),
(37, 37, 80, '2018-05-25 00:00:00', '2018-03-15 00:00:00', 0),
(38, 38, 46704874.819066, '2018-06-12 00:00:00', '2018-08-10 00:00:00', 0),
(39, 39, 4.2941, '2018-05-28 00:00:00', '2018-10-20 00:00:00', 0),
(40, 40, 12114751.8, '2018-11-03 00:00:00', '2018-07-06 00:00:00', 2),
(41, 1, 527, '2018-08-01 00:00:00', '2018-02-03 00:00:00', 2),
(42, 2, 39884911.5, '2018-09-11 00:00:00', '2018-08-31 00:00:00', 1),
(43, 3, 0, '2018-11-12 00:00:00', '2018-07-23 00:00:00', 2),
(44, 4, 1.202, '2018-05-16 00:00:00', '2018-10-25 00:00:00', 1),
(45, 5, 4309530.2, '2018-10-05 00:00:00', '2018-12-05 00:00:00', 1),
(47, 7, 5408, '2018-06-12 00:00:00', '2018-09-02 00:00:00', 1),
(48, 8, 14501794.011975, '2018-11-05 00:00:00', '2018-05-25 00:00:00', 2),
(49, 9, 1.0232792, '2018-06-04 00:00:00', '2018-05-29 00:00:00', 2),
(50, 10, 1897.02, '2018-10-14 00:00:00', '2018-05-10 00:00:00', 2);


CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `users` (`id`, `first_name`, `last_name`, `user_name`) VALUES
(1, 'Josefa', 'Torphy', 'cartwright.alivia@hotmail.com'),
(2, 'Easter', 'Tremblay', 'karli.harris@yahoo.com'),
(3, 'Madeline', 'Swift', 'jewell31@hotmail.com'),
(4, 'Everett', 'Predovic', 'mekhi.stiedemann@hotmail.com'),
(5, 'Johnnie', 'Hettinger', 'wcorwin@gmail.com'),
(6, 'Mose', 'Ratke', 'vbarton@yahoo.com'),
(7, 'Burnice', 'Simonis', 'marlene.zieme@yahoo.com'),
(8, 'Melvin', 'Mosciski', 'dudley.kub@yahoo.com'),
(9, 'Marlon', 'Graham', 'dooley.cloyd@yahoo.com'),
(10, 'Destini', 'Runte', 'clint.klocko@hotmail.com'),
(11, 'Trent', 'Kuphal', 'hegmann.sylvia@yahoo.com'),
(12, 'Kristofer', 'Stark', 'sallie78@hotmail.com'),
(13, 'Makayla', 'Howell', 'kkrajcik@hotmail.com'),
(14, 'Roosevelt', 'Grant', 'cristian.stracke@hotmail.com'),
(15, 'Hans', 'Gerhold', 'earline.mcdermott@gmail.com'),
(16, 'Cornelius', 'Bode', 'hollie39@gmail.com'),
(17, 'Lexus', 'Schultz', 'august.cremin@yahoo.com'),
(18, 'Fredrick', 'Romaguera', 'mireille50@yahoo.com'),
(19, 'Helen', 'Corwin', 'schinner.rosendo@gmail.com'),
(20, 'Rasheed', 'Wyman', 'lind.jamal@hotmail.com'),
(21, 'Emilio', 'Mosciski', 'kaia58@hotmail.com'),
(22, 'Lera', 'Tremblay', 'darrion78@gmail.com'),
(23, 'Herminio', 'Schmidt', 'elaina.erdman@hotmail.com'),
(24, 'Eldred', 'Welch', 'bennie45@hotmail.com'),
(25, 'Rylee', 'Kertzmann', 'josephine.homenick@hotmail.com'),
(26, 'Nettie', 'Sanford', 'lindgren.earl@gmail.com'),
(27, 'Marcia', 'Schmidt', 'nolan.madilyn@gmail.com'),
(28, 'Kamren', 'Hessel', 'oweber@hotmail.com'),
(29, 'Evalyn', 'Bogisich', 'chandler59@hotmail.com'),
(30, 'Vicenta', 'Lynch', 'martin45@hotmail.com'),
(31, 'Giles', 'Armstrong', 'aufderhar.anne@hotmail.com'),
(32, 'Zander', 'Will', 'donato.davis@hotmail.com'),
(33, 'Luisa', 'O\'Reilly', 'lcrist@gmail.com'),
(34, 'Christa', 'Reichert', 'fannie.reynolds@hotmail.com'),
(35, 'Helga', 'Osinski', 'karli27@hotmail.com'),
(36, 'Gino', 'Metz', 'metz.amiya@yahoo.com'),
(37, 'Bradly', 'Legros', 'amber.morar@yahoo.com'),
(38, 'Milton', 'Pagac', 'breana.zieme@hotmail.com'),
(39, 'Bell', 'Emmerich', 'lourdes.ondricka@hotmail.com'),
(40, 'Malika', 'Kris', 'ehoeger@hotmail.com');

ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_name` (`user_name`);

ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;
