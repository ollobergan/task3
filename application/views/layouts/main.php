<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Управление заказами</title>
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo site_url('assets/css/bootstrap.min.css');?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo site_url('assets/css/font-awesome.min.css');?>">
    <link rel="shortcut icon" href="<?php echo site_url('assets/img/')?>favicon.ico" type="image/x-icon" />
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="container">
    <section class="content">
        <?php
        if(isset($_view) && $_view)
            $this->load->view($_view);
        ?>
    </section>
</div>
<!-- jQuery 2.2.3 -->
<script src="<?php echo site_url('assets/js/jquery-2.2.3.min.js');?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo site_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?php echo site_url('assets/DataTables/datatables.js');?>"></script>
<script>
    $(document).ready(function() {
        //call initialize function
        setupViewJS();
    });
</script>
</body>
</html>
