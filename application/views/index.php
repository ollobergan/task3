<br>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Управление заказами</h3>
    </div>
    <div class="panel-body">
        <table class="table table-bordered" id="datatable_orders">
            <thead>
            <tr>
                <td><span class="fa fa-sort-numeric-asc"></span> №</td>
                <td><span class="fa fa-user"></span> Пользователь </td>
                <td><span class="fa fa-user"></span> Номер заказа</td>
                <td><span class="fa fa-money"></span> Сумма заказа </td>
                <td><span class="fa fa-calendar"></span> Дата оплаты </td>
                <td><span class="fa fa-calendar"></span> Дата создания </td>
                <td><span class="fa fa-line-chart"></span> Статус заказа </td>
                <td><span class="fa fa-cog"></span> Управление</td>
            </tr>
            </thead>
        </table>
    </div>
</div>
<!-- Modal for action confirmation -->
<div class="modal modal-default fade" id="modal-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Подтверждение</h4>
            </div>
            <div class="modal-body text-center">
                <h2 id="message">Изменить статус?</h2>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Нет </button>
                <button type="button" class="btn btn-success" id="yes_btn">Да </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal modal-default fade" id="modal-user">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Информация о пользователе</h4>
            </div>
            <div class="modal-body ">
                <div id="info-user">
                    <h4><b>Username: </b><span id="user_name_info"></span></h4>
                    <h4><b>First name: </b><span id="first_name_info"></span></h4>
                    <h4><b>Last name: </b><span id="last_name_info"></span></h4>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"> Закрыть</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script type="text/javascript">
    //function for buttons confirmation in Управление
    function confirmation(id,action) {
        //base ulr for ajax
        var url='<?=base_url()?>orders/';
        //which confirmation delete or change status
        if(action==0){
            //action is delete
            $("#message").text("Удалить заказ?");
            url+="delete/"+id;

        }else {
            //action is change status
            $("#message").text("Изменить статус?");
            url+="change_status/"+id;
        }

        //show confirmation modal
        $("#modal-confirm").modal();
        //click button yes
        $('#yes_btn').unbind("click").click(function(){
            $.ajax({
                url: url,
                type: "POST",
                success: function(result){
                },
                complete:function(result){
                    //confirmation close modal
                    $("#modal-confirm").modal('hide');
                    //reload datatable for new data
                    $('#datatable_orders').DataTable().ajax.reload();
                },
                error: function(jqXHR,  textStatus,  errorThrown){alert("ошибка о действии: "+textStatus);},
            });
        });
    }
    // function for user information in Пользователь
    function user_info(id) {
        //base ulr for ajax
        var url='<?=base_url()?>users/user_info/'+id;

        $.ajax({
            url: url,
            contentType: "application/json",
            success: function(result){
                $("#user_name_info").html(result.user_name);
                $("#first_name_info").html(result.first_name);
                $("#last_name_info").html(result.last_name);
                $("#modal-user").modal();
            },
            error: function(jqXHR, textStatus,  errorThrown){
                alert("ошибка о действии: "+textStatus);
            },
        });


    }

    //Main funtion for initialize
    function setupViewJS(){
        var datatable_url="<?php echo site_url('orders/orders_list')?>";
        //initialize datatable
        $('#datatable_orders').dataTable({
            "autoWidth" : true,
            //Russian localization
            "oLanguage": {
                "sSearch":"Поиск: _INPUT_ ",
                "sProcessing": "Подождите...",
                "sLengthMenu": "Показать _MENU_ записей",
                "sInfo": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "sInfoEmpty": "Записи с 0 до 0 из 0 записей",
                "sInfoFiltered": "(отфильтровано из _MAX_ записей)",
                "sInfoPostFix": "",
                "sLoadingRecords": "Загрузка записей...",
                "sZeroRecords": "Записи отсутствуют.",
                "sEmptyTable": "В таблице отсутствуют данные",
                "oPaginate": {
                    "sFirst": "Первая",
                    "sPrevious": "Предыдущая",
                    "sNext": "Следующая",
                    "sLast": "Последняя"
                },
                "oAria": {
                    "sSortAscending": ": активировать для сортировки столбца по возрастанию",
                    "sSortDescending": ": активировать для сортировки столбца по убыванию"
                }
            },
            // get data with ajax for datatable
            "ajax": {
                "url": datatable_url,
                "type": "POST",
                "dataType": 'json',
                "Content-Type" :"application/json",
            },
            //define table columns data
            "columns": [
                { "data": "no"},
                { "data": "user_name"},
                { "data": "id"},
                { "data": "order_price"},
                { "data": "date_of_payment"},
                { "data": "date_of_creation"},
                { "data": "order_status"},
                { "data": "action" },
            ],
            "serverSide": true,
            "processing":true,
            "ordering": false
        });
        $('.dataTables_filter input').attr("placeholder", "введите имя пользователя или номер заказа");
        $('.dataTables_filter input').css(
            {'width':'350px','display':'inline-block'}
        );;
    }
</script>
