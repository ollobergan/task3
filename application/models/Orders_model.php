<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//Class for working db orders table
class Orders_model extends CI_Model
{

    function __construct(){
        parent::__construct();
    }
    //Function for datatble data
    public function get_orders_list(){
        //check a search key
        if($_POST['search']['value'])
        {
            //call mysql procedure orders_list_filtered. this procedure return filtered data from orders table
            $query=$this->db->query("CALL orders_list_filtered(?,?,?)", [$_POST['start'],$_POST['length'],$_POST['search']['value']]);
        }else{
            //call mysql procedure orders_list. this procedure return all orders data
            $query=$this->db->query("CALL orders_list(?,?)", [$_POST['start'],$_POST['length']]);
        }
        $result=$query->result_array();
        mysqli_next_result( $this->db->conn_id );
        //Frees memory from query result
        $query->free_result();
        return $result;

    }
    //function for datatable statistics. Function returned count of filtered result from all orders
    public function count_filtered_orders()
    {
        if($_POST['search']['value'])
        {
            $query = $this->db->query("CALL count_filtered_orders(?)",[$_POST['search']['value']]);
            $result =$query->row_array();
            mysqli_next_result( $this->db->conn_id );
            $query->free_result();
            return $result['filtered_count'];
        }else{
            return $this->count_all_orders();
        }

    }
    //function for datatable statistics. Function returned count of all orders
    public function count_all_orders()
    {
        //call mysql procedure count_all_orders. this procedure return count all orders
        $query = $this->db->query("CALL count_all_orders()");
        $result =$query->row_array();
        mysqli_next_result( $this->db->conn_id );
        $query->free_result();
        return $result['all_count'];
    }
    //function for deleting order.
    public function delete_order($id){
        $this->db->delete('orders', array('id' => $id));
    }
    //function for change status order.
    //This function change status step by step: Не оплачен -> Оплачен -> Отправлен
    public function change_status($id){
        $this->db->set('order_status', 'order_status+1', FALSE);
        $this->db->where('id', $id);
        $this->db->update('orders');
    }
}
