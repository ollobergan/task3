<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model');// Loading model Users_model
    }

    //function for get user information
    public function user_info($id)
    {
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($this->Users_model->get_user_info($id), JSON_UNESCAPED_UNICODE));
    }
}
