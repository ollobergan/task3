<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Orders_model'); //Loading orders model
    }

    //Main function of orders controller. This function open the index page
    public function index()
    {
        $data['_view'] = 'index';
        $this->load->view('layouts/main',$data);
    }
    //Function for datatable fill data. This function reurn json data for ajax request
    public function orders_list()
    {
        //get orders list
        $order_list = $this->Orders_model->get_orders_list();
        //$data variable for return result
        $data = array();
        //datatable numeration
        $no = $_POST['start'];
        //loop for modify data for datatable
        foreach ($order_list as $order) {
            $no++;
            $row = array();
            $row['no'] = $no;
            $row['user_name']="<a href='#' class='text-primary' onclick='user_info(".$order['user_id'].")'>".$order['user_name']."</a>";
            $row['id']=$order['id'];
            $row['order_price']=number_format($order['order_price'],2,'.',' ');
            $row['date_of_payment']=$this->relative_time(strtotime($order['date_of_payment'],false));
            $row['date_of_creation']=$this->relative_time(strtotime($order['date_of_creation'],false));
            //order acrions
            $row['action'] ='<button class="btn btn-danger btn-sm" onclick="confirmation('.$order['id'].',0)"><span class="fa fa-remove"></span> Удалить</button> <bR>';
            //The switch statement for action buttons
            switch($order['order_status']){
                case 0:
                    // add button Изменить статус на оплачен and add status style
                    $row['order_status']="<span class='label label-warning'>Не оплачен</span>";
                    $row['action'] .='<button class="btn btn-primary btn-sm" onclick="confirmation('.$order['id'].',1)"><span class="fa fa-check-circle"></span>Изменить статус на оплачен</button>';
                    break;
                case 1:
                    // add button Изменить статус на отправлен and add status style
                    $row['order_status']="<span class='label label-primary'>Оплачен</span>";
                    $row['action'] .='<button class="btn btn-primary btn-sm" onclick="confirmation('.$order['id'].',1)"><span class="fa fa-check-circle"></span>Изменить статус на отправлен</button>';
                    break;
                case 2:
                    //remove button for change status
                    $row['order_status']="<span class='label label-success'>Отправлен</span>";
                    break;
            }
            $data[] = $row;
        }
        //information for datatable
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Orders_model->count_all_orders(),
            "recordsFiltered" => $this->Orders_model->count_filtered_orders(),
            "data" => $data,
        );
        //convert datatype to json
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($output, JSON_UNESCAPED_UNICODE));


    }
    //function for modify date time
    public function relative_time($time, $short = false){
        $SECOND = 1;
        $MINUTE = 60 * $SECOND;
        $HOUR = 60 * $MINUTE;
        $DAY = 24 * $HOUR;
        $MONTH = 30 * $DAY;
        $before = time() - $time;

        if ($before < 0)
        {
            return "not yet";
        }

        if ($short){
            if ($before < 1 * $MINUTE)
            {
                return ($before <5) ? "только что" : $before . " назад";
            }

            if ($before < 2 * $MINUTE)
            {
                return "1 минут назад";
            }

            if ($before < 45 * $MINUTE)
            {
                return floor($before / 60) . " минут назад";
            }

            if ($before < 90 * $MINUTE)
            {
                return "1 час назад";
            }

            if ($before < 24 * $HOUR)
            {

                return floor($before / 60 / 60). " час назад";
            }

            if ($before < 48 * $HOUR)
            {
                return "1 день назад";
            }

            if ($before < 30 * $DAY)
            {
                return floor($before / 60 / 60 / 24) . " день назад";
            }


            if ($before < 12 * $MONTH)
            {
                $months = floor($before / 60 / 60 / 24 / 30);
                return $months <= 1 ? "1 месяц назад" : $months . " месяц назад";
            }
            else
            {
                $years = floor  ($before / 60 / 60 / 24 / 30 / 12);
                return $years <= 1 ? "1 год назад" : $years." год назад";
            }
        }

        if ($before < 1 * $MINUTE)
        {
            return ($before <= 1) ? "только что" : $before . " секунд назад";
        }

        if ($before < 2 * $MINUTE)
        {
            return "1 минут назад";
        }

        if ($before < 45 * $MINUTE)
        {
            return floor($before / 60) . " минут назад";
        }

        if ($before < 90 * $MINUTE)
        {
            return " час назад";
        }

        if ($before < 24 * $HOUR)
        {

            return (floor($before / 60 / 60) == 1 ? 'около часа' : floor($before / 60 / 60).' часов '). " назад";
        }

        if ($before < 48 * $HOUR)
        {
            return "вчера";
        }

        if ($before <= 8 * $DAY)
        {
            return "Неделю назад";
        }

        if ($before < 30 * $DAY)
        {
            return floor($before / 60 / 60 / 24) . " дней назад";
        }

        if ($before < 12 * $MONTH)
        {

            $months = floor($before / 60 / 60 / 24 / 30);
            return $months <= 1 ? "месяц назад" : $months . " месяц назад";
        }
        else
        {
            $years = floor  ($before / 60 / 60 / 24 / 30 / 12);
            return $years <= 1 ? "год назад" : $years." лет назад";
        }

        return "$time";
    }

    //function for change status order
    public function change_status($id){
        $this->Orders_model->change_status($id);
    }

    //function for delete order
    public function delete($id){
        $this->Orders_model->delete_order($id);
    }
}